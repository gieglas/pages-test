Pages-test 
===============

Simple js app that load media files on the page. The app uses jquery and boostrap and also uses `.gitlab-ci.yml` to autopmatically deploy on gitlap pages. 

The whole js logic is in `js/app.js` and the UI content is controlled by `data/local.json`. 

The purpose of this app is to make a template for future demos and tests.