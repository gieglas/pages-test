$(window).on('load', function() {      
    var mediaLinks = null;
    //initialize stuff and load data on shell
    $.getJSON( "data/local.json", function( data ) {        
        $('#navBrand').html(data.brand);
        $('#mainTitle').html(data.title);
        $('#mainText').html(data.text);
        $('#mainFooter').html(data.footer);
        mediaLinks = data.mediaLinks;
        var mainNavItems ="";
        $.each( mediaLinks, function( index, value ){
        mainNavItems += '<li class="nav-item"><a class="nav-link" href="#" id="'+value.id+'">'+value.text+'</a></li>';
        });
        $('#mainNavItems').html(mainNavItems);
    });

    //click on any of the links
    $('#mainNav').on('click', '.nav-link', function(elem) {
        var id = this.id;
        if(!mediaLinks[id]["direct"]) {
            $("#mainImage").html("<img class='img-fluid' src='" + mediaLinks[id]["url"] + "?t="+Date.now()+"'>");
            //toggle active in navbar
            $("#mainNav").find(".active").removeClass("active");
            $(this).parent().addClass("active");
        } else {
            window.location.href = mediaLinks[id]["url"];
        }
    });
    //click on nav brand
    $('#mainNav').on('click', '.navbar-brand', function(elem) {        
        $("#mainImage").html("");
    });
});